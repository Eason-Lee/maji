//
//  StoreTests.swift
//  MajiTestTests
//
//  Created by lizhuojie on 2020/8/22.
//  Copyright © 2020 maji. All rights reserved.
//

import XCTest

@testable import MajiTest

class StoreTests: XCTestCase {

    override func setUpWithError() throws {
        Store.shared.removeAll()
        Store.shared.saveToDatabase(data: ["foo0": "bar0"], ts: 0)
        Store.shared.saveToDatabase(data: ["foo10": "bar10"], ts: 10)
        Store.shared.saveToDatabase(data: ["foo20": "bar20"], ts: 20)
    }

    override func tearDownWithError() throws {
        Store.shared.removeAll()
    }

    func testReadData() throws {
        let method = "Store#getData"
        var result: [String: String]?
        let exp = self.expectation(description: method)
        Store.shared.getData(0) { (value) in
            result = value
            exp.fulfill()
        }
        wait(for: [exp], timeout: 1)
        XCTAssertNotNil(result, "\(method) 数据读取错误")
        XCTAssertEqual(result!["foo0"], "bar0", "\(method) 返回数据错误")
    }

    func testGetList() throws {
        let method = "Store#getData"
        var result1: [Int64]? // 向后读取
        var result2: [Int64]? // 向后读取
        let exp1 = self.expectation(description: method + "1")
        let exp2 = self.expectation(description: method + "2")
        Store.shared.getList(0, direction: .backward) { (values) in
            result1 = values
            exp1.fulfill()
        }
        Store.shared.getList(21, direction: .forward, count: 2) { (values) in
            result2 = values
            exp2.fulfill()
        }
        
        wait(for: [exp1, exp2], timeout: 1)
        
        XCTAssertNotNil(result1, "\(method)1 数据读取错误")
        XCTAssertEqual(result1!.count, 2, "1读取数量错误")
        XCTAssertEqual(result1![0], 20, "1读取值错误")
        
        XCTAssertNotNil(result2, "\(method)2 数据读取错误")
        XCTAssertEqual(result2!.count, 2, "2读取数量错误")
        XCTAssertEqual(result2![0], 20, "2读取值错误")
    }
}
