//
//  RootViewController.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/18.
//  Copyright © 2020 maji. All rights reserved.
//

import UIKit

class RootViewController: UITableViewController {

    private let viewModel: RootViewModel = RootViewModel()
    
    /// 由历史记录跳转过来
    var timeStamp: Int64?
    
    var showUpdateTips: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.config(vc: self, ts: timeStamp)
    }
    
    func reloadData() {
        showUpdateTips = timeStamp == nil && !viewModel.sortedKeys.isEmpty
        tableView.reloadData()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showUpdateTips ? viewModel.sortedKeys.count + 1 : viewModel.sortedKeys.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell
    
        if indexPath.row == 0 && showUpdateTips {
            cell = tableView.dequeueReusableCell(withIdentifier: "UpdateTipsCell", for: indexPath)
            let date = Date(timeIntervalSince1970: TimeInterval(viewModel.lastUpdateTime))
            cell.textLabel?.text = "最后更新: \(date)"
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "RootTableViewCell", for: indexPath)
            if let rc = cell as? RootTableViewCell, !viewModel.sortedKeys.isEmpty {
                let key = viewModel.sortedKeys[showUpdateTips ? indexPath.row - 1 : indexPath.row]
                rc.keyLabel.text = key
                rc.valueLabel.text = viewModel.apiData[key]
            }
        }
        
        return cell
    }
}
