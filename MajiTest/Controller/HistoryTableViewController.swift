//
//  HistoryTableViewController.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/18.
//  Copyright © 2020 maji. All rights reserved.
//

import UIKit

import SnapKit
import RxSwift
import RxCocoa
import MJRefresh

class HistoryTableViewController: UITableViewController {
    
    private let viewModel: HistoryViewModel = HistoryViewModel()
    
    private var tipsButton: UIButton = UIButton(type: .custom)
    
    private var bag: DisposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        buildUI()
        
        viewModel.config(self)
        viewModel.loadData(.forward)
    }
    
    private func buildUI() {
        configTipsButton()
        configRefresh()
    }

    private func configTipsButton() {
        view.addSubview(tipsButton)
        tipsButton.setTitle("有新的数据", for: .normal)
        tipsButton.setTitleColor(.blue, for: .normal)
        tipsButton.backgroundColor = .lightGray
        tipsButton.clipsToBounds = true
        tipsButton.layer.cornerRadius = 6
        tipsButton.isHidden = true
        tipsButton.layer.zPosition += 1
        
        tipsButton.rx.tap
            .subscribe(onNext: { [weak self] (_) in
                self?.tipsButton.isHidden = true
                self?.tableView.setContentOffset(.zero, animated: true)
                self?.tableView.mj_header?.beginRefreshing()
            }).disposed(by: bag)
        
        // 固定tips button位置
        tableView.rx.contentOffset
            .subscribe(onNext: { [unowned self] (offset) in
                self.tipsButton.snp.remakeConstraints({ (make) in
                    make.centerX.equalToSuperview()
                    make.top.equalToSuperview().offset(offset.y + 158)
                    make.size.equalTo(CGSize(width: 118, height: 44))
                })
            }).disposed(by: bag)
    }
    
    private func configRefresh() {
        tableView.mj_header = MJRefreshNormalHeader(refreshingBlock: { [weak self] in
            self?.tipsButton.isHidden = true
            self?.viewModel.loadData(.backward)
        })
        tableView.mj_footer = MJRefreshAutoFooter(refreshingBlock: { [weak self] in
            self?.viewModel.loadData(.forward)
        })
    }
    
    func showTipsIfNeed() {
        guard !(tableView.mj_header?.isRefreshing ?? false) else {
            return
        }
        tipsButton.isHidden = false
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.timestampList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BaseTableViewCell", for: indexPath)

        let ts = TimeInterval(viewModel.timestampList[indexPath.row])
        let text = Date(timeIntervalSince1970: ts).description
        if indexPath.row == 0 && viewModel.hasLatestData {
            cell.textLabel?.text = "最新数据: \(text)"
        } else {
            cell.textLabel?.text = text
        }

        return cell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard
            let cell = sender as? UITableViewCell,
            let indexPath = tableView.indexPath(for: cell),
            let root = segue.destination as? RootViewController
            else { return }
        
        root.title = cell.textLabel?.text
        root.navigationItem.rightBarButtonItems = []
        root.timeStamp = viewModel.timestampList[indexPath.row]
    }

}
