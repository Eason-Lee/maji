//
//  RootTableViewCell.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/18.
//  Copyright © 2020 maji. All rights reserved.
//

import UIKit

class RootTableViewCell: UITableViewCell {

    @IBOutlet var keyLabel: UILabel!
    @IBOutlet var valueLabel: UILabel!
    
}
