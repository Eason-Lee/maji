//
//  Store.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/18.
//  Copyright © 2020 maji. All rights reserved.
//

import Foundation

import SQLite
import RxSwift

fileprivate let kDidCreateHistoryTables = "kDidCreateHistoryTables"

/// 分页读取数据方向
enum PageDirection {
    /// 向前读取更早的数据
    case forward
    /// 向后读取更新的数据
    case backward
}

// MARK: - Store

class Store {
    
    static let shared = Store()
    
    fileprivate var db: Connection?
    
    fileprivate let queue = DispatchQueue(label: "com.maji.github.api.db.queue")
    
    private let dbPath: String = {
        let document = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        return "\(document)/db.sqlite3"
    }()
    
    var dataChanged: PublishSubject<Int64> = PublishSubject<Int64>()
    
    private init() {
        
        let dbExists = FileManager.default.fileExists(atPath: dbPath)
        
        do {
            db = try Connection(dbPath)
        } catch {
            print("数据库连接失败: \(dbPath)")
            return
        }
        
        if !dbExists || !UserDefaults.standard.bool(forKey: kDidCreateHistoryTables) {
            do {
                _ = try db?.run(HistoryColumns.create())
                UserDefaults.standard.set(true, forKey: kDidCreateHistoryTables)
                UserDefaults.standard.synchronize()
            } catch {
                print("数据库初始化失败: \(error)")
            }
        }
    }
    
    // 移除所有数据（测试用）
    func removeAll() {
        queue.async {
            guard let db = self.db else { return }
            let delete = HistoryColumns.table.delete()
            do {
                _ = try db.run(delete)
                print("\(Date()) 移除所有缓存数据。")
            } catch {
                print("移除数据失败: \(error)")
            }
        }
    }
    
    // 保存数据
    func saveToDatabase(data: [String: String], ts: Int64) {
        queue.async {
            guard let db = self.db else { return }
            
            do {
                let json = try JSONSerialization.data(withJSONObject: data, options: .withoutEscapingSlashes)
                let setters: [Setter] = [
                    HistoryColumns.timestamp <- ts,
                    HistoryColumns.data <- String(data: json, encoding: .utf8) ?? "",
                ]
                
                let insert = HistoryColumns.table.insert(or: .replace, setters)
                _ = try db.run(insert)
                
                self.dataChanged.onNext(ts)
            } catch {
                print("保存数据失败: \(error)")
            }
        }
    }
    
    /// 读取缓存列表，默认单次20条，倒序
    /// - Parameters:
    ///   - timestamp: 启示时间节点
    ///   - count: 单次读取最大数量
    ///   - direction: 读取方向
    ///   - completionHandler: 回调
    func getList(_ timestamp: Int64,
                 direction: PageDirection,
                 count: Int = 20,
                 completionHandler: @escaping ([Int64]) -> Void
    ) {
        queue.async {
            guard let db = self.db else { completionHandler([]); return }
            
            var query: Table = HistoryColumns.table
                .select([HistoryColumns.timestamp])
                .order(HistoryColumns.timestamp.desc)
                .limit(count)
            
            switch direction {
            case .backward:
                query = query.filter(HistoryColumns.timestamp > timestamp)
            case .forward:
                query = query.filter(HistoryColumns.timestamp < timestamp)
            }

            do {
                let values = try db.prepare(query)
                var list: [Int64] = []
                for row in values {
                    list.append(row[HistoryColumns.timestamp])
                }
                completionHandler(list)
            } catch {
                print("数据读取失败: \(error)")
                completionHandler([])
            }
        }
    }
    
    func getData(_ timestamp: Int64, completionHandler: @escaping ([String: String]?) -> Void) {
        queue.async {
            guard let db = self.db else { completionHandler(nil); return }
            do {
                let query = HistoryColumns.table.filter(HistoryColumns.timestamp == timestamp)
                
                guard
                    let row = try db.prepare(query).first(where: { _ in true }),
                    let data = row[HistoryColumns.data].data(using: .utf8)
                    else {
                        completionHandler(nil)
                        return
                }
                
                let dict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: String]
                completionHandler(dict)
            } catch {
                print("数据读取失败: \(error)")
                completionHandler(nil)
            }
        }
    }
}

// MARK: - Table

struct HistoryColumns {
    
    static let table = Table("history")
    
    static let timestamp = Expression<Int64>("timestamp")
    
    static let data = Expression<String>("data")
    
    static func create() -> String {
        return table.create(ifNotExists: true) { (t) in
            t.column(timestamp, primaryKey: .default)
            t.column(data)
        }
    }
}
