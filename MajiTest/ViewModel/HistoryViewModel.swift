//
//  HistoryViewModel.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/21.
//  Copyright © 2020 maji. All rights reserved.
//

import Foundation

import RxSwift

class HistoryViewModel {
    
    weak var controller: HistoryTableViewController?
    
    var timestampList: [Int64] = []
    
    private var updateTime: Int64 = 0
    
    /// 包含最新一条调用
    var hasLatestData: Bool = false
    
    /// 是否还有更多数据
    private var hasMore: Bool = true
    
    /// 是否正在读取
    private var isLoading: Bool = false
    
    private var bag: DisposeBag = DisposeBag()
    
    func config(_ vc: HistoryTableViewController) {
        controller = vc
        
        Store.shared.dataChanged
            .observeOn(MainScheduler.instance)
            .subscribe(onNext: { [unowned self] (updateTime) in
                self.updateTime = updateTime
                if self.hasLatestData {
                    self.hasLatestData = false
                    self.controller?.tableView.reloadData()
                }
                self.controller?.showTipsIfNeed()
            }).disposed(by: bag)
    }
    
    func loadData(_ direction: PageDirection) {
        guard !isLoading else { return }
        isLoading = true
        
        var timestamp: Int64
        switch direction {
        case .forward:
            timestamp = timestampList.last ?? Int64.max
        case .backward:
            timestamp = timestampList.first ?? 0
        }
        
        Store.shared.getList(timestamp, direction: direction) { [unowned self] (values) in
            
            switch direction {
            case .backward:
                self.timestampList = values + self.timestampList
            case .forward:
                self.timestampList.append(contentsOf: values)
            }

            DispatchQueue.main.async {
                self.isLoading = false
                
                self.controller?.tableView.mj_header?.endRefreshing()
                self.controller?.tableView.mj_footer?.endRefreshing()
                
                guard !values.isEmpty else {
                    if direction == .backward {
                        self.hasMore = false
                    }
                    return
                }
                
                self.hasLatestData = (self.timestampList.first ?? 0) >= self.updateTime
                self.controller?.tableView.reloadData()
            }
        }
    }
}
