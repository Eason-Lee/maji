//
//  RootViewModel.swift
//  MajiTest
//
//  Created by lizhuojie on 2020/8/21.
//  Copyright © 2020 maji. All rights reserved.
//

import Foundation
import Alamofire

class RootViewModel {
    
    var apiData: [String: String] = [:]
    var sortedKeys: [String] = []
    
    weak var controller: RootViewController?
    
    private var timeStamp: Int64?

    var lastUpdateTime: Int64 = 0
    
    func config(vc: RootViewController, ts: Int64?) {
        controller = vc
        timeStamp = ts
        prepareData()
    }
    
    // MAKR: - Data
    
    private func prepareData() {
        if let ts = timeStamp {
            loadFromDatabase(ts)
        } else {
            loadLastData()
            startTimer()
        }
    }
    
    private func startTimer() {
        let fireTime = Date(timeIntervalSinceNow: 5) // 5秒后启动
        let timer = Timer(fire: fireTime, interval: 5, repeats: true, block: { [weak self] (timer) in
            print("\(Date()): 开始加载数据")
            self?.loadFromGithub()
        })
        RunLoop.current.add(timer, forMode: .common)
    }
    
    // 启动读取最后一次请求的数据
    private func loadLastData() {
        Store.shared.getList(Int64.max, direction: .forward, count: 1) { [weak self] (values) in
            guard let lastTimestamp = values.first else { return }
            print("\(Date()): 读取最后一次请求的数据")
            self?.loadFromDatabase(lastTimestamp)
        }
    }
    
    private func loadFromDatabase(_ ts: Int64) {
        Store.shared.getData(ts) { [weak self] (data) in
            guard let data = data else { return }
            self?.apiData = data
            self?.sortedKeys = data.keys.sorted()
            DispatchQueue.main.async {
                self?.lastUpdateTime = ts
                self?.controller?.reloadData()
            }
        }
    }
    
    private func loadFromGithub() {
        AF.request("https://api.github.com/").responseJSON { [weak self] (response) in
            switch response.result {
            case .success(let data as [String: String]):
                self?.apiData = data
                self?.sortedKeys = data.keys.sorted()
                let ts = Int64(Date().timeIntervalSince1970)
                DispatchQueue.main.async {
                    self?.lastUpdateTime = ts
                    self?.controller?.reloadData()
                }
                Store.shared.saveToDatabase(data: data, ts: ts)
            case .failure(let error):
                print("请求数据失败: \(error)")
            case .success(_):
                print("Github数据格式变更。")
            }
        }
    }
}
