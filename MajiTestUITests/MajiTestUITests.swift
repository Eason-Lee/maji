//
//  MajiTestUITests.swift
//  MajiTestUITests
//
//  Created by lizhuojie on 2020/8/18.
//  Copyright © 2020 maji. All rights reserved.
//

import XCTest

@testable import MajiTest

class MajiTestUITests: XCTestCase {

    override func setUpWithError() throws {

        continueAfterFailure = false
        
        Store.shared.removeAll()
    }

    override func tearDownWithError() throws {
        Store.shared.removeAll()
    }
    
    func testHomeView() {
        let app = XCUIApplication()
        app.launch()
        
        let first = app.cells.staticTexts["authorizations_url"]
        XCTAssertTrue(first.exists)
        XCTAssertTrue(first.waitForExistence(timeout: 5))
        
        let update = app.cells.staticTexts.containing(NSPredicate(format: "label CONTAINS '最后更新'")).firstMatch
        XCTAssertTrue(update.exists)
        
        self.expectation(for: NSPredicate(format: "label != '\(update.label)'"), evaluatedWith: update, handler: nil)
        waitForExpectations(timeout: 15, handler: nil) // 数据是否更新
        
        app.tables.firstMatch.swipeUp()
        app.tables.firstMatch.swipeUp()
        app.tables.firstMatch.swipeDown()
        app.tables.firstMatch.swipeDown()
    }

    func testHistory() throws {
        let app = XCUIApplication()
        app.launch()
                
        app.navigationBars["Github API"].buttons["History"].tap() // 进入历史记录列表
                
        let refreshButton = app.buttons["有新的数据"]
        self.expectation(for: NSPredicate(format: "hittable == true"), evaluatedWith: refreshButton, handler: nil)
        waitForExpectations(timeout: 6, handler: nil) // 数据更新未与历史记录联动
        
        refreshButton.tap()
        
        app.cells.firstMatch.tap() // 进入历史记录详情
        app.buttons["History"].tap()
        
        self.expectation(for: NSPredicate(format: "hittable == true"), evaluatedWith: refreshButton, handler: nil)
        waitForExpectations(timeout: 6, handler: nil)
        refreshButton.tap()
        
        app.navigationBars["History"].buttons["Github API"].tap() // 返回首页
    }
}
